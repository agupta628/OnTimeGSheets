var webHookURL = "https://maker.ifttt.com/trigger/reminder/with/key/ct6p6W_232bEKEdkipWB90"
var emailAddress = "rukna1000@gmail.com"

var dataRawSheetIndex = 0;

var eventIndex = 0;
var deadlineIndex = 1;
var pushNotifications = 3;

function remind() {
	var sheet = SpreadsheetApp.getActiveSpreadsheet().getSheets()[dataRawSheetIndex];
	var data = sheet.getDataRange().getValues();

	var eventsToRemind = [];

	var today = new Date();
	var currentMonth = today.getMonth();
	var currentDate = today.getDate();

	for(var i = 1; i < data.length; i++) {
		var eventName = data[i][eventIndex];
		var deadlineDate = data[i][deadlineIndex];

		for(var j = pushNotifications + 1; j < data[0].length; j++) {
			reminderDate = advancedDate(deadlineDate, data[0][j]);

			if(currentMonth == reminderDate.getMonth() && currentDate == reminderDate.getDate()) {
				eventsToRemind.push(data[i]);
				break;
			}
		}
	}

	pushReminders(eventsToRemind);
}

function pushReminders(events) {
	if(events.length == 0) {
		return;
	}

	var today = new Date();
	var currentMonth = today.getMonth();
	var currentDate = today.getDate();

	var subject = "Your Reminders for " + currentDate + "/" + (currentMonth + 1);
	var message = "Your reminders for the following month: \n\n"

		for(var i = 0; i < events.length; i++) {
			var eventName = events[i][eventIndex];
			var date = events[i][deadlineIndex];
			var push = events[i][pushNotifications];

			var line = "* " + eventName + " on " + date + "\n";
			message = message + line;

			if(push == 1) {
				var data = {
					'value1' : eventName.toString(),
					'value2' : date.toString().slice(4, 11),
					'value3' : 'null'
				};

				var payload = JSON.stringify(data);

				var options = {
					'method' : 'post',
					'contentType' : 'application/json',
					'payload' : payload
				};

				UrlFetchApp.fetch(webHookURL, options);
			}
		}

	message = message + "\nEnjoy being OnTime!";

	MailApp.sendEmail(emailAddress, subject, message);
}

function advancedDate(deadlineArr, reminderAdvance) {
	reminderAdvance = reminderAdvance.toLowerCase();

	var count = parseInt(reminderAdvance);
	var newDate = new Date(deadlineArr.getTime());  

	if(reminderAdvance.indexOf("day") > -1) {
		newDate.setDate(newDate.getDate() - count);
	}
	else if(reminderAdvance.indexOf("week") > -1) {
		newDate.setDate(newDate.getDate() - count * 7);
	}
	else if(reminderAdvance.indexOf("month") > -1) {
		newDate.setMonth(newDate.getMonth() - count);
	}

	return newDate;
}
